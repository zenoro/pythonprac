def fib(m,n):
    from itertools import islice
    def f():
        pa = 1
        fu = 1
        while 1:
            yield pa
            pa, fu = fu, fu + pa
    return (islice(f(),m,n+m))

exec(input())
