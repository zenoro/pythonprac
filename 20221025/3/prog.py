print(*["".join(q) for q in sorted(list(filter(lambda x: x.count('TOR') == 2, map(lambda x: ''.join(x), __import__("itertools").product('TOR', repeat=int(input()))))))], sep=', ')
