from collections import UserString

class DivStr(UserString):
    def __init__(self, arg=''):
        super().__init__(arg)
    def __floordiv__(self, n):
        step = len(self)//n
        return iter(self[i*step:(i+1)*step] for i in range(n))
    def __mod__(self, n):
        return self[-(len(self) % n):]
        
import sys
exec(sys.stdin.read())
