class Undead(Exception): pass
class Skeleton(Undead): pass
class Zombie(Undead): pass
class Ghoul(Undead): pass

def necro(a):
    rest = a % 3
    d = [Skeleton, Zombie, Ghoul]
    raise d[rest]

x,y = eval(input())
for a in range(x,y):
    try:
        necro(a)
    except Skeleton:
        print('Skeleton')
    except Zombie:
        print('Zombie')
    except Undead:
        print('Generic Undead')
