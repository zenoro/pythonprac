from math import hypot, sqrt

class InvalidInput(Exception): pass
class BadTriangle(Exception): pass


def triangleSquare(inStr):
    try:
        (x1, y1), (x2, y2), (x3, y3) = eval(inStr)
    except:
        raise InvalidInput
    a1 = hypot(x1 - x2, y1 - y2)
    a2 = hypot(x2 - x3, y2 - y3)
    a3 = hypot(x1 - x3, y1 - y3)
    if a1 < a2 + a3 and a3 < a2 + a1 and a2 < a1 + a3 and all((a1, a2, a3)):
        p = (a1 + a2 + a3) / 2
        s = sqrt(p*(p-a1)*(p-a2)*(p-a3))
        if round(s,6) != 0:
            return s
        else:
            raise BadTriangle
    else:
        raise BadTriangle

try:
    while s:=input():
        try:
            k = triangleSquare(s)
        except InvalidInput:
            print("Invalid input")
        except BadTriangle:
            print("Not a triangle")
        else:
            print('%.2f' % k)
except EOFError:
    pass
