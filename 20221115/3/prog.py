class Alpha:
    __slots__ = list("abcdefghijklmnopqrstuvwxyz")

    def __init__(self, **kwargs):
        for i in kwargs:
            setattr(self, i, kwargs[i])
    
    def __str__(self):
        res = []
        for i in self.__slots__:
            a = getattr(self,i,"")
            if a != "":
                res.append(": ".join([i, str(a)]))
        return ", ".join(res)


class AlphaQ:
    stopper = list("abcdefghijklmnopqrstuvwxyz")

    def __init__(self, **kwargs):
        for i in kwargs:
            if i in AlphaQ.stopper:
                self.__dict__[i] = kwargs[i]
            else:
                raise AttributeError
    
    def __setattr__(self, name, value):
        if name in AlphaQ.stopper:
            self.__dict__[name] = value
        else:
            raise AttributeError

    def __getattr__(self, name):
        if name in AlphaQ.stopper and getattr(self.__dict__, name, "#$%@") != "#$%@":
            return self.__dict__[name]
        else:
            raise AttributeError

    def __str__(self):
        res = []
        for i in sorted(self.__dict__.keys()):
            res.append(f"{i}: {self.__dict__[i]}")
        return ", ".join(res)

import sys
exec(sys.stdin.read())
