def objcount(cls):
    cls.counter = 0
    clsinit = cls.__init__

    def init(self, *args):
        cls.counter += 1
        clsinit(self, *args)
    cls.__init__ = init

    def delit(self):
        cls.counter -= 1
        if hasattr(cls.__dict__, '__del__'):
            cls.__del__(self)
    cls.__del__ = delit
    return cls

import sys
exec(sys.stdin.read())