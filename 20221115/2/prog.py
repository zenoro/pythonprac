class Num:
    def __set__(self, obj, val):
        if hasattr(val, "real"):
            obj._value = val.real
        else:
            obj._value = len(val)

    def __get__(self, obj, cls):
        return obj._value if hasattr(obj, "_value") else 0    


import sys
exec(sys.stdin.read())
