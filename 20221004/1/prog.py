def recs(a,b):
    if list(b) != sorted(b):
        return False
    k=len(b)//2
    if a==b[k] or a in b and k<=1:
        return True
    else:
        if k==1:
            return False
        else:
            if a in b[k:]:
                return(recs(a,b[k:]))
            else:
                return(recs(a,b[:k]))

x1,x2=eval(input())
print(recs(x1,x2))