cup = input()
g, w = 0, 0
count = -1

# counting
while 1:
    s = input()
    if (cup == s):
        break
    w += sum([i == "~" for i in s]) 
    g += sum([i == "." for i in s])
    count += 1


# drawing
print("#"*(count+2))
for i in range(len(cup)-2):
	if g - i * count > 0:
		print("#", "." * count, "#", sep="")
	else:
		print("#", "~" * count, "#", sep="")
print("#" * (count+2))

# write statistic
addable = max(w, g)
denum = g + w
print(("." * g).ljust(addable), f"{g}/{denum}", sep="")
print(("~"*w).ljust(addable), f"{w}/{denum}", sep = "")