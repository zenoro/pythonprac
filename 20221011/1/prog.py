def Fractions(*args):
    from fractions import Fraction
    s=args[0]
    w=args[1]
    degA=args[2]
    coefA=[]
    for i in range(degA+1):
        coefA.append(args[i+3])
    degB=args[3+degA]
    coefB=list(args[5+degA:])
    numer=sum([coefA[-i]*s**(i-1) for i in range(1,degA+2)])
    denumer=sum([coefB[-i]*s**(i-1) for i in range(1,degB+2)])
    return Fraction(numer/denumer) == Fraction(w) if denumer != 0 else False

arr=eval(input())
print(Fractions(*arr))
