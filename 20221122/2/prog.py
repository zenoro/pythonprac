import sys, codecs
into = sys.stdin.read()
codecs.register_error('?', UnicodeEncodeError)
buf = codecs.encode(into, 'latin1', errors='replace')
codecs.register_error('?', UnicodeDecodeError)
buf = codecs.decode(buf, 'cp1251', errors='replace')
sys.stdout.write(buf)
