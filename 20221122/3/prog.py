from struct import unpack
import sys

into = sys.stdin.buffer.read()


if len(into) < 44:
    print('NO')
else:
    # print(unpack('4s',into[12:16])[0])

    checkbuf = [unpack('4s',into[:4])[0] == b'RIFF', unpack('4s',into[8:12])[0] == b'WAVE', unpack('4s',into[12:16])[0] == b'fmt ']
    if all(checkbuf):
        Size = unpack('i', into[4:8])[0]
        Type = unpack("h", into[20:22])[0]
        Channels = unpack("h", into[22:24])[0]
        Rate = unpack("i", into[24:28])[0]
        Bits = unpack("h", into[34:36])[0]
        Datasize = unpack("i", into[40:44])[0]
        print(f'{Size=}, {Type=}, {Channels=}, {Rate=}, {Rate=}, {Bits=}, Data size={Datasize}')
    else:
        print("NO")
