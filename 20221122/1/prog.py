import sys

into = sys.stdin.buffer.read()
fst, data = into[0], into[1:]
step = len(data) / fst
res = []
for i in range(fst):
    temp = data[round(i * step):round((i + 1) * step)]
    if temp:
        res.append(temp)
sys.stdout.buffer.write(into[:1] + b"".join(sorted(res)))
