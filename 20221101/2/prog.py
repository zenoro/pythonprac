class Triangle:
    
    def __init__(self, a, b, c):
        self.x1, self.y1 = a
        self.x2, self.y2 = b
        self.x3, self.y3 = c

    def __abs__(self):
        from math import hypot
        a = hypot(self.x1 - self.x2, self.y1 - self.y2)
        b = hypot(self.x2 - self.x3, self.y2 - self.y3)
        c = hypot(self.x1 - self.x3, self.y1 - self.y3)
        if a + b > c or b + c > a or a + c > b:
            p = (a + b + c) / 2
            self.S = (p * (p - a) * (p - b) * (p - c))**0.5
        else:
            self.S = 0
        return round(self.S, 5)
    
    def __bool__(self):
        return round(self.__abs__(), 8) != 0
    
    def __lt__(self, other):            # <
        if isinstance(other, Triangle):
            return self.__abs__() < other.__abs__()

    def __contains__(self, other):      # in
        if isinstance(other, Triangle):
            if other.__abs__() and self.__abs__():
                res = []
                for i in range(1,4):
                    xt, yt = eval(f'other.x{i}, other.y{i}')
                    l1 = (self.x1 - xt) * (self.y2 - self.y1) - (self.x2 - self.x1) * (self.y1 - yt)
                    l2 = (self.x2 - xt) * (self.y3 - self.y2) - (self.x3 - self.x2) * (self.y2 - yt)
                    l3 = (self.x3 - xt) * (self.y1 - self.y3) - (self.x1 - self.x3) * (self.y3 - yt)
                    res.append(l1 * l2 >= 0 and l2 * l3 >= 0 and l1 * l3 >= 0)
                return all(res)
            else:
                return True

    def __and__(self, other):           # &
        if isinstance(other, Triangle):
            if bool(self) and bool(other):
                def f(self, it):
                    x, y = it
                    a = (self.x1 - x) * (self.y2 - self.y1) - (self.x2 - self.x1) * (self.y1 - y)
                    b = (self.x2 - x) * (self.y3 - self.y2) - (self.x3 - self.x2) * (self.y2 - y)
                    c = (self.x3 - x) * (self.y1 - self.y3) - (self.x1 - self.x3) * (self.y3 - y)
                    return a * b >= 0 and b * c >= 0 and a * c >= 0
    
                def checker(other, self):
                    op1, op2, op3 = (other.x1, other.y1), (other.x2, other.y2), (other.x3, other.y3)
                    k = not (f(self, op1) and f(self,op2) and f(self,op3))
                    k2 = f(self, op1) or f(self, op2) or f(self, op3)
                    return k and k2

                return checker(self, other) or checker(other, self)
            else:
                return False


exec(__import__('sys').stdin.read())
