class Omnibus:
    res = dict()

    def __init__(self, *args):
        pass

    def __setattr__(self, name: str, value):
        if not name.startswith("_"):
            if Omnibus.res.get(name) == None:
                Omnibus.res[name] = set()
            Omnibus.res[name].add(self)
            self.res.update(Omnibus.res)

    def __getattr__(self, attr: str):
        if not attr.startswith("_"):
            return len(self.res[attr])

    def __delattr__(self, name: str):
        if self in self.res.get(name, {}):
            self.res[name] -= {self}


exec(__import__('sys').stdin.read())