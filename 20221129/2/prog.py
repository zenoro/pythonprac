import types

class check(type):
    def __init__(cls, name, parents, ns, **kwds):
        # print("init", cls, parents, ns, kwds)
        mydict = cls.__annotations__
        def check_annotations(self):
            for k in mydict:
                try:
                    kk = type(getattr(self, k))
                except:
                    return False
                typeofattr = mydict[k]
                if isinstance(typeofattr, types.GenericAlias):
                    typeofattr = typeofattr.__origin__
                if not kk == typeofattr and not issubclass(kk, typeofattr):
                    return False
            return True
        setattr(cls, "check_annotations", check_annotations)
        return super().__init__(name, parents, ns)

import sys
exec(sys.stdin.read())