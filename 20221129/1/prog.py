import types

def decor(f):
    def ff(self, *args, **kwargs):
        print(f"{f.__name__}: {args}, {kwargs}")
        return f(self, *args, **kwargs)
    return ff


class dump(type):
    def __new__(mtcls, clsname, clspar, ns, **kwargs):
        for i in ns:
            if callable(ns[i]):
                ns[i] = decor(ns[i])
        return super().__new__(mtcls, clsname, clspar, ns)

import sys
exec(sys.stdin.read())