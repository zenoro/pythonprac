def puti(dirbuf):
    res=0
    if len(dirbuf)!=0:
        for i in dirbuf:
            match i:
                case "right":
                    res+=1
                case "left":
                    res-=1
                case _:
                    return "Error in travelling: wrong direction!"
        return "<-traveled" if res<0 else "traveled->" 
    else:
        return "Nowhere to travel!"


class Spell():
    def __init__(self, tt, ss):
        self.typee = tt
        self.strength = ss
        if self.strength == 0:
            return "Some spell of zero strength"
        match self.typee:
            case "lightning":
                return f"Electric damage of {self.strength} hit points"
            case "fireball":
                return f"Fire damage of {self.strength} hit points"
            case _:
                return f"Unknown spell of type {self.typee}, use magic with care!" 


while s := input("> "):
    match s.lower().split():
        case ["about"]:
            print("MUD version 0.01")
        case ["credits", "--year"]:
            print("Copyright (c) developers 2022")
        case ["credits"]:
            print("Copyright (c) developers")
        case ['move',direction]:
            match direction:
                case "right":
                    print('moved ->')
                case "left":
                    print("moved <-")
                case _:
                    print("Wrong direction!")
        case ['travel', *dirdir]:
            print(puti(dirdir))
        case ["quit"]:
            break
        case _:
            print("Cannot Parse")
            
