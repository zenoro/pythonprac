s=input()
res=[]
for i in range(1,len(s)):
    if s[i].isalpha() and s[i-1].isalpha():
        res.append("".join([s[i-1].lower(),s[i].lower()]))
print(len(set(res)))