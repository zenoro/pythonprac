import math

fd={}
res=[]
linecounter=1
funccounter=1

while (s:=input().split())[0] != "quit":
    linecounter+=1
    if s[0][0] == ":":    #function
        fd[s[0][1:]] = (s[1:-1] , s[-1])
        funccounter+=1
    else:
        if len(s)>1:
            args = {fd[s[0]][0][i]: eval(s[1:][i]) for i in range(len(s[1:]))}
            res.append((eval(fd[s[0]][1], vars(math), args)))
        else:
            res.append(fd[s[0]][1])

print(*res, sep="\n")
print(" ".join(s[1:]).format(funccounter,linecounter).strip("\""))


