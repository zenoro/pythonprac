import re
from collections import Counter

w=int(input())
text=""
while s:=input().strip():
    text += re.sub(r"\W"," ", s) + " "
text=re.sub(r"\d"," ",text)

text = Counter(text.lower().split())

maxx=0
res=[]
for i in text.most_common(len(text)):
    if len(i[0]) == w:
        if maxx <= i[1]:
            maxx=i[1]
            res.append(i[0])
        else:
            break
        
print(*sorted(res), sep=" ") if res != [] else print("")
