a=input()
if len(a)==0:
    print('[]')

elif not ',' in a:
    print([int(a)])
else:
    a=list(eval(a))
    for i in range(len(a)):
        for j in range(len(a) - 1):
            if (a[j]**2) % 100 > (a[j+1]**2) % 100:
                a[j], a[j+1] = a[j+1], a[j]
    print(a)
