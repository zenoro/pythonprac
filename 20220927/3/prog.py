def mat_input(matrix,matrixlength,ranger):
    for i in range(ranger):
        s=list(eval(input()))
        if len(s)==matrixlength:
            matrix.append(s)
        else:
            raise ValueError("dimension crashed but you tried")
    return matrix

A=[]
B=[]
s=list(eval(input()))
A.append(s)
matrixlen=len(s)
res=[[0 for i in range(matrixlen)] for j in range(matrixlen)]

A=mat_input(A,matrixlen,(matrixlen-1))
B=mat_input(B,matrixlen,matrixlen)

for i in range(matrixlen):
    for j in range(matrixlen):
        for k in range(matrixlen):
            res[i][j] += A[i][k] * B[k][j]
    print(*res[i], sep=',')
